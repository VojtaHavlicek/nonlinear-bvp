/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  a solution to simple bvp (pendulum) based on LeVeque's book on 
 * 		    numerical methods
 *
 *        Version:  1.0
 *        Created:  01/07/13 11:20:00
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   
 *   Organization:  is the oposite to entropy!
 *
 * =====================================================================================
 */

#include "main.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  entropy = anarchy 
 * =====================================================================================
 */
int main ( int argc, char *argv[] )
{
    init();
    log(1);

    iterate();
    log(2);
    
    iterate();
    log(3);

    iterate();
    log(4);

    iterate();
    log(5);

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  init
 *  Description:  Initialization phase of the execution. Allocates memory to all the 
 *  		  neccessary vectors and initializes them.
 * =====================================================================================
 */
void init()
{
    // Memory allocation	
    jacobian_diagonal = gsl_vector_alloc(N);
    jacobian_sub_diag = gsl_vector_alloc(N-1);
    increments	  = gsl_vector_alloc(N);
    rhs_values 	  = gsl_vector_alloc(N);
    
    data_points   = gsl_vector_alloc(N+2); // data points containing the initial + final bc.

    // Take a line connecting the Dirichlet boundary conditions as an initial guess
    double value_ = 0.0;
   
    for(int i = 0; i < N+2; i++){
	value_ = i*(double (final_theta-init_theta)/T) + init_theta;
    	gsl_vector_set(data_points, i, value_);
    }

}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  iterate
 *  Description:  Performs an iteration towards the solution
 * =====================================================================================
 */
void iterate()
{
    // Set initial values
    gsl_vector_set_all(jacobian_sub_diag, 1/(h*h)); // for subdiagonal
    
    // Prepare the jacobian diagonal for the problem
    int i;
    double next_t_ = 0;
    double t_      = 0;
    double prev_t_ = 0;

    for(i = 0; i < N; i++)
    {
    	// TODO: this is clumsy... How about just shifting the indices?
    	prev_t_ = gsl_vector_get(data_points, i);
	     t_ = gsl_vector_get(data_points, i+1); // avoiding endpoints by shifting the index 	
	next_t_ = gsl_vector_get(data_points, i+2);

	gsl_vector_set(jacobian_diagonal, i, -2/(h*h) + cos(t_));
	gsl_vector_set(rhs_values, i, iteration_schema(next_t_, t_, prev_t_)); // sets the rhs
    }

    
    // Solve the system for linear increments
    gsl_linalg_solve_symm_tridiag(jacobian_diagonal, jacobian_sub_diag, rhs_values, increments);


    // Update data points. TODO: Again, this seems a bit clumsy way of doing so.
    for(i = 0; i < N; i++)
    {
	double* val_ = gsl_vector_ptr(data_points, i+1); // avoiding init/final points
 	      *val_ += gsl_vector_get(increments, i);
    }

}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  iteration_schema
 *  Description:  Returns a discrete approximation to the problem at given step
 * =====================================================================================
 */
double iteration_schema(double next_t, double t, double prev_t)
{
	return -(1/(h*h))*(next_t - 2*t + prev_t) - sin(t);
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  log
 *  Description:  Logs off the content of data_points
 * =====================================================================================
 */
void log(int id)
{
    std::stringstream s_id_;
    s_id_ << id;

    std::string file_string_ ="p" + s_id_.str()+".dat";
    FILE *f = fopen(file_string_.c_str(), "w");
    gsl_vector_fprintf(f,data_points,"%.5g");
    fclose(f);
}
