
/*-----------------------------------------------------------------------------
 *  main.h
 *-----------------------------------------------------------------------------*/

#define _USE_MATH_DEFINES

/*-----------------------------------------------------------------------------
 *  INCLUDES:
 *-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *  GSL includes:
 *-----------------------------------------------------------------------------*/
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>


/*-----------------------------------------------------------------------------
 *  Common includes:
 *-----------------------------------------------------------------------------*/
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>

/*-----------------------------------------------------------------------------
 *  VARIABLES:
 *-----------------------------------------------------------------------------*/
double T = 2*M_PI;	       // Simulation period
   int N = 200;		       // Number of iterations in the problem
double h = T/(N+1);	       // Mesh-width

double  init_theta = M_PI/4;    // Initial angle
double final_theta = M_PI/4;   // Final angle

gsl_vector* jacobian_diagonal; // The matrix of the problem is tridiagonal, symmetric
gsl_vector* jacobian_sub_diag; // so sub = sup.
gsl_vector* increments;        // Increment between consecutive steps
gsl_vector* data_points;       // Theta values
gsl_vector* rhs_values;	       // RHS values.

/*-----------------------------------------------------------------------------
 *  FUNCTIONS:
 *-----------------------------------------------------------------------------*/
void init();      // Allocates all the neccessary vectors
void iterate();   // Performs an iteration towards the solution
void log(int id); // Logs off the content of data_points to a file p[id].dat

double iteration_schema(double next_t, double t, double prev_t); // Returns a discrete approx. to a step in the problem

